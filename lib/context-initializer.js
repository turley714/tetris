export default function initializeCtx(selector) {
  let canvas = document.querySelector(selector)
  canvas.width = canvas.parentElement.offsetWidth;
  canvas.height = canvas.parentElement.offsetHeight;
  return canvas.getContext('2d');
}

