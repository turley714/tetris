const VELOCITY = 3;

export default class Square {
  constructor(size, position) {
    this.size = size;
    this.x = position.x;
    this.y = position.y;
  }

  update() {
    if ((this.y + this.size) < 901) {
      this.y += VELOCITY;
    }
  }

  draw(ctx) {
    ctx.fillStyle = '#FF0000'
    ctx.fillRect(this.x, this.y, this.size, this.size);
  }
}
