import setStatus from './lib/status.js'
import Square from './lib/square.js'
import initializeCtx from './lib/context-initializer.js'

const ROW_LENGTH = 10;
const FPS = 60;

class App {
  constructor() {
    this.main = initializeCtx('#main canvas');
    this.side = initializeCtx('#side canvas');
    this.gridSize = this.main.canvas.width / ROW_LENGTH;

    let squares = [new Square(this.gridSize, { x: 0, y: 0 })];
    this._loop(squares);
  }

  _loop(squares) {
    console.log('frame rendered');
    this.main.fillStyle = '#FFFFFF'
    this.main.fillRect(0, 0, this.main.canvas.width, this.main.canvas.height);

    squares.forEach((square) => {
      square.update();
      square.draw(this.main);
    });

    setTimeout(() => { this._loop(squares) }, 1000 / FPS);
  }
}

new App()
